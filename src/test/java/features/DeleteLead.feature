Feature: Delete a Lead profile

Scenario Outline: Delete Lead
Given Enter the username as <userName>
And Enter the password as <password>
And click Login button
And click CRM/SFA
And click Leads
And click FindLeads
And click PhoneTab
And enter PhoneNum as <phoneCode> and <phoneNum>
And click FindLeads button
And get FirstResultId
And click first result
When click Delete
And click FindLeads
Then enter Deleted Id
And click FindLeads button
And verify NoRecords
		
Examples:
|userName|password|phoneCode|phoneNum|
|DemoSalesManager|crmsfa|043|9486877788|