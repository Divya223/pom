Feature: Duplicate a Lead profile

Scenario Outline: Duplicate Lead
Given Enter the username as <userName>
And Enter the password as <password>
And click Login button
And click CRM/SFA
And click Leads
And click FindLeads
And click Email tab
And enter Email as <email>
And click FindLeads button
And get First result Name
And click first result
And click Duplicate
And verify PageTitle
When click DupCreate
Then verifyFirstName		

Examples:
|userName|password|email|
|DemoSalesManager|crmsfa|divya223@gmail.com|