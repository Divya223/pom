Feature: Create a Lead profile

#Background:
#Given Open the Browser
#And Max the Browser
#And Set the Timeout
#And Launch the URL

Scenario Outline: Flow1
And Enter the username as <userName>
And Enter the password as <password>
And click Login button
And click CRM/SFA
And click Leads
And click CreateLead
And Enter CompanyName as <companyName>
And Enter FirstName as <firstName>
And Enter LastName as <lastName>
When click CreateLead button
Then verify the firstName as <verifyfirstName>

Examples:
|userName|password|companyName|firstName|lastName|verifyfirstName|
|DemoSalesManager|crmsfa|IBM|Udhaya|Kumar|Udhaya|

#Scenario: Flow2
#And Enter the username as DemoSalesManager
#And Enter the password as crmsfa
#And click Login button
#And click CRM/SFA
#And click Leads
#And click CreateLead
#And Enter CompanyName as Accenture
#And Enter FirstName as George
#And Enter LastName as David
#When click CreateLead button
#Then verify the firstName as George
