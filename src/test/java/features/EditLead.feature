Feature: Edit a Lead profile

Scenario Outline: Edit Lead
And Enter the username as <userName>
And Enter the password as <password>
And click Login button
And click CRM/SFA
And click Leads
And click FindLeads		
And Enter firstName as <fName>
And click FindLeads button
And click first result
And verify Page title
When click Edit
And edit CompanyName as <companyName> 
And click update
Then verify the companyName as <verifycompanyName>

Examples:
|userName|password|fName|companyName|verifycompanyName|
|DemoSalesManager|crmsfa|Divya|Verizon|Verizon|
