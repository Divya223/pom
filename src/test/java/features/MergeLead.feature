Feature: Merge 2 Lead profiles

Scenario Outline: Merge Lead
And Enter the username as <userName>
And Enter the password as <password>
And click Login button
And click CRM/SFA
And click Leads
And click MergeLeads
And click From Id icon as <newIndex>
And Enter FromLead Id as <fromId>
And click FindLeads button
And click From First result as <parentIndex>
And click To Id icon as <newIndex>
And Enter ToLead Id as <toId>
And click FindLeads button
And click To First result as <parentIndex>
When click Merge button
And accept the alert
And click FindLeads
And Enter verifying Lead Id as <fromId>
And click FindLeads button
Then verify NoRecords

Examples:
|userName|password|fromId|toId|newIndex|parentIndex|
|DemoSalesManager|crmsfa|10084|10085|1|0|