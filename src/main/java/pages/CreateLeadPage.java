package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="createLeadForm_companyName") WebElement compName;
	@FindBy(id="createLeadForm_firstName") WebElement fName;
	@FindBy(id="createLeadForm_lastName") WebElement lName;
	@FindBy(className="smallSubmit") WebElement buttonCreate;
	
	@Given("Enter CompanyName as (.*)")
	public CreateLeadPage enterCompName(String data) {
		type(compName, data);
		return this;
	}
	
	@Given("Enter FirstName as (.*)")
	public CreateLeadPage enterfirstName(String data) {
		type(fName, data);		
		return this;
	}
	
	@Given("Enter LastName as (.*)")
	public CreateLeadPage enterlastName(String data) {
		type(lName, data);
		return this;
	}
	
	@When("click CreateLead button")
	public ViewLeadPage clickCreateLead() {
		
		click(buttonCreate);
		return new ViewLeadPage();
	}
	

}
