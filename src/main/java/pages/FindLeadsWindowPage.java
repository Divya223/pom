package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class FindLeadsWindowPage extends ProjectMethods{

	public FindLeadsWindowPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//input[@name='id']") WebElement leadId;
	@FindBy(xpath="//button[text()='Find Leads']") WebElement findLeadsbtn;
	@FindBy(xpath="(//a[@class='linktext'])[1]") WebElement firstResult;
	

	@Given("Enter FromLead Id as (.*)")	
	public FindLeadsWindowPage enterFromId(String data) {
		type(leadId,data);
		return this;
	}
	
	@Given("Enter ToLead Id as (.*)")
	public FindLeadsWindowPage enterToId(String data) {
		type(leadId,data);
		return this;
	}
	public FindLeadsWindowPage clickFindLeadsBtn() {
		click(findLeadsbtn);
		return this;
	}

	@Given("click From First result as (.*)")
	public MergeLeadsPage clickFromFirstResult(String index) {
		click(firstResult);
		switchToWindow(Integer.parseInt(index));
		return new MergeLeadsPage();
	}
	@Given("click To First result as (.*)")
	public MergeLeadsPage clickToFirstResult(String index) {
		click(firstResult);
		switchToWindow(Integer.parseInt(index));
		return new MergeLeadsPage();
	}


}
