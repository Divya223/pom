package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="viewLead_firstName_sp") WebElement vlfName;
	@FindBy(linkText="Find Leads") WebElement findLeads;
	@FindBy(linkText="Edit") WebElement eleEdit;
	@FindBy(id="viewLead_companyName_sp") WebElement companyName;
	@FindBy(linkText="Duplicate Lead") WebElement eleDuplicate;
	@FindBy(xpath="//span[@id='viewLead_firstName_sp']") WebElement firstName;
	@FindBy(linkText="Delete") WebElement eleDelete;



	@Then("verify the firstName as (.*)")
	public ViewLeadPage verifyFname(String data) {
		verifyExactText(vlfName,data);
		return this;
	}
	//check if this can be taken from MyLeadsPage
	public FindLeadsPage clickFindLeads() {
		click(findLeads);
		return new FindLeadsPage();
	}

	@Given("verify Page title")
	public ViewLeadPage verifyPageTitle() {
		String title = getTitle();
		System.out.println(title);
		verifyTitle(title);
		return this;
	}

	@When("click Edit")
	public EditLeadPage clickEdit() {
		click(eleEdit);
		return new EditLeadPage();
	}

	@Then("verify the companyName as (.*)")
	public ViewLeadPage getcompanyName(String data) {
		String text = companyName.getText();
		if(text.contains(data)) {
			System.out.println("New name updated");
		}
		else System.out.println("Name not updated");
		return this;
	}

	@Given("click Duplicate")
	public DuplicateLeadPage clickDuplicate() {
		click(eleDuplicate);
		return new DuplicateLeadPage();
	}

	@Then("verifyFirstName")
	public ViewLeadPage verifyFirstName() {
		verifyExactText(firstName, FindLeadsPage.name1);
		return this;
	}

	@When("click Delete")
	public MyLeadsPage clickDelete() {
		click(eleDelete);
		return new MyLeadsPage();	
	}
}
