package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="updateLeadForm_companyName") WebElement companyName;
	@FindBy(xpath="(//input[@class='smallSubmit'])[1]") WebElement updateBtn;


	@When("edit CompanyName as (.*)")
	public EditLeadPage editCompanyName(String data) {
		companyName.clear();
		type(companyName,data);
		return this;
	}
	
	@When("click update")
	public ViewLeadPage clickUpdate() {
		click(updateBtn);
		return new ViewLeadPage();	
	}
}
