package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//input[@name='firstName'])[3]") WebElement firstName;
	@FindBy(xpath="//input[@name='id']") WebElement leadId;
	@FindBy(xpath="//button[text()='Find Leads']") WebElement findLeadsBtn;
	@FindBy(xpath="//div[text()='No records to display']") WebElement noRecords;
	@FindBy(xpath="(//a[@class='linktext'])[4]") WebElement firstResult;
	@FindBy(xpath="//span[text()='Email']") WebElement emailBtn;
	@FindBy(xpath="//input[@name='emailAddress']") WebElement emailField;
	@FindBy(xpath="(//a[@class='linktext'])[6]") WebElement emailFirstName;
	@FindBy(xpath="//span[text()='Phone']") WebElement elePhoneTab;
	@FindBy(xpath = "//input[@name='phoneAreaCode']") WebElement PhoneCode;
	@FindBy(xpath="//input[@name='phoneNumber']") WebElement PhoneNum;
	static String name1;
	static String id1;

	@Given("Enter firstName as (.*)")
	public FindLeadsPage enterFirstName(String fName) {
		type(firstName,fName);
		return this;
	}

	@When("Enter verifying Lead Id as (.*)")
	public FindLeadsPage enterFromLeadId(String data) {
		type(leadId,data);
		return this;
	}

	@Given("click FindLeads button")
	public FindLeadsPage clickFindLeadsBtn(){
		click(findLeadsBtn);
		return this;
	}

	@Then("verify NoRecords")
	public FindLeadsPage verifyNoRecords() {
		verifyDisplayed(noRecords);
		return this;
	}

	@Given("click first result")
	public ViewLeadPage clickFirstResult() {
		click(firstResult);
		return new ViewLeadPage();
	}

	@Given("click Email tab")
	public FindLeadsPage clickEmail() {
		click(emailBtn);
		return this;
	}

	@Given("enter Email as (.*)")
	public FindLeadsPage enterEmail(String data) {
		type(emailField,data);
		return this;
	}

	@Given("get First result Name")
	public FindLeadsPage getFirstEmailName() {
		name1 = emailFirstName.getText();
		return this;
	}
	
	@Given("click PhoneTab")
	public FindLeadsPage clickPhoneTab() {
		click(elePhoneTab);
		return this;
	}
	@Given("enter PhoneNum as (.*) and (.*)")
	public FindLeadsPage enterPhoneNum(String data1, String data2) {
		type(PhoneCode, data1);
		type(PhoneNum, data2);
		return this;
	}
	
	@Given("get FirstResultId")
	public FindLeadsPage getFirstResultId() {
		id1 = firstResult.getText();
		return this;
	}
	
	@Then("enter Deleted Id")
	public FindLeadsPage enterDeletedId() {
		type(leadId,id1);
		return this;		
	}
}
