package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText="Create Lead") WebElement eleCL;
	@FindBy(linkText="Find Leads") WebElement eleFindLeads;
	@FindBy(linkText="Merge Leads") WebElement eleMergeLeads;

	@Given("click CreateLead")
	public CreateLeadPage clickCreateLead() {
		click(eleCL);
		return new CreateLeadPage();
	}
	
	@Given("click FindLeads")
	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		return new FindLeadsPage();
	}
	
	@Given("click MergeLeads")
	public MergeLeadsPage clickMergeLeads() {
		click(eleMergeLeads);
		return new MergeLeadsPage();

	}







}
