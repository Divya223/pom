package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//img[@src='/images/fieldlookup.gif'])[1]") WebElement fromIcon;
	@FindBy(xpath="(//img[@src='/images/fieldlookup.gif'])[2]") WebElement toIcon;
	@FindBy(linkText="Merge") WebElement mergeBtn;


	@Given("click From Id icon as (.*)")
	public FindLeadsWindowPage clickFrom(String index) {
		click(fromIcon);
		switchToWindow(Integer.parseInt(index));
		return new FindLeadsWindowPage();
	}
	
	@Given("click To Id icon as (.*)")
	public FindLeadsWindowPage clickTo(String index) {
		click(toIcon);
		switchToWindow(Integer.parseInt(index));
		return new FindLeadsWindowPage();
	}
	
	@When("click Merge button")
	public MergeLeadsPage clickMergeBtn() {
		click(mergeBtn);
		return this;
	}
	
	@When("accept the alert")
	public ViewLeadPage acceptnewAlert() {
		acceptAlert();
		return new ViewLeadPage();
	}
}
