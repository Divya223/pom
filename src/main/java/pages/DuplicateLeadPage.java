package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.core.sym.Name1;
import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods{

	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="(//input[@class='smallSubmit'])[1]") WebElement eleCreate;

	@Given("verify PageTitle")
	public DuplicateLeadPage verifyPageTitle() {
		String title = getTitle();
		verifyTitle(title);
		return this;
	}
	
	@When("click DupCreate")
	public ViewLeadPage clickDupCreate() {
		click(eleCreate);
		return new ViewLeadPage();
	}
}
