package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_Createlead";
		testDescription = "Create a Lead";
		authors = "Divya";
		category = "smoke";
		dataSheetName = "TC002_CreateLead";
		testNodes = "CreateLead";
	}
	@Test(dataProvider="fetchData")
	public void create(String userName, String password,String cName, String fName, String lName) {

		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickCreateLead()
		.enterCompName(cName)
		.enterfirstName(fName)
		.enterlastName(lName)
		.clickCreateLead()
		.verifyFname(fName);
		System.out.println("The element "+fName+" verified successfully");
	}

}
