package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import pages.ViewLeadPage;
import wdMethods.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_Mergelead";
		testDescription = "Merge 2 Leads";
		authors = "Divya";
		category = "smoke";
		dataSheetName = "TC003_MergeLead";
		testNodes = "MergeLead";
	}
	@Test(dataProvider="fetchData")
	public void merge(String userName, String password,String fromId, String toId, String newIndex, String parentIndex) {

		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickMergeLeads() 
		.clickFrom(newIndex)
		.enterFromId(fromId)
		.clickFindLeadsBtn()
		.clickFromFirstResult(parentIndex)
		.clickTo(newIndex)
		.enterToId(toId)
		.clickFindLeadsBtn()
		.clickToFirstResult(parentIndex)
		.clickMergeBtn()
		.acceptnewAlert()
		.clickFindLeads()
		.enterFromLeadId(fromId)
		.clickFindLeadsBtn()
		.verifyNoRecords(); 
	}

}
