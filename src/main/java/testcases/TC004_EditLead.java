package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC004_Editlead";
		testDescription = "Edit a Lead profile";
		authors = "Divya";
		category = "smoke";
		dataSheetName = "TC004_EditLead";
		testNodes = "EditLead";
	}
	@Test(dataProvider="fetchData")
	public void merge(String userName, String password,String fName,String companyName) {

		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(fName)
		.clickFindLeadsBtn()
		.clickFirstResult()
		.verifyPageTitle()
		.clickEdit()
		.editCompanyName(companyName)
		.clickUpdate()
		.getcompanyName(companyName);		
	}
}
