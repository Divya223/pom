package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import pages.ViewLeadPage;
import wdMethods.ProjectMethods;

public class TC005_DuplicateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC005_Duplicatelead";
		testDescription = "Duplicate a Lead profile";
		authors = "Divya";
		category = "smoke";
		dataSheetName = "TC005_DuplicateLead";
		testNodes = "DuplicateLead";
	}
	@Test(dataProvider="fetchData")
	public void merge(String userName, String password,String email) {

		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickFindLeads()
		.clickEmail()
		.enterEmail(email)
		.clickFindLeadsBtn()
		.getFirstEmailName()
		.clickFirstResult()
		.clickDuplicate()
		.verifyPageTitle()
		.clickDupCreate()
		.verifyFirstName();		
	}
}
