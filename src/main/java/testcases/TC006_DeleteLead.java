package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import pages.ViewLeadPage;
import wdMethods.ProjectMethods;

public class TC006_DeleteLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC006_Deletelead";
		testDescription = "Delete a Lead profile";
		authors = "Divya";
		category = "smoke";
		dataSheetName = "TC006_DeleteLead";
		testNodes = "DeleteLead";
	}
	@Test(dataProvider="fetchData")
	public void merge(String userName, String password,String phoneCode, String phoneNum){

		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickFindLeads()
		.clickPhoneTab()
		.enterPhoneNum(phoneCode, phoneNum)
		.clickFindLeadsBtn()
		.getFirstResultId()
		.clickFirstResult()
		.clickDelete()
		.clickFindLeads()
		.enterDeletedId()
		.clickFindLeadsBtn()
		.verifyNoRecords();
	}
}
